# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields, Unique


class MeterReading(ModelSQL, ModelView):
    "Meter Reading Definition"
    __name__ = "maintenance.meter_reading"
    _rec_name = 'name'
    code = fields.Char('Code', required=True)
    name = fields.Char('Name', required=True)
    uom_reading = fields.Many2One('product.uom', 'UoM Reading',
            required=True)
    cost_value = fields.Numeric('Cost Value', digits=(16, 2))
    type_input = fields.Selection([
            ('incremental', 'Incremental'),
            ('diferencial', 'Diferencial'),
            ], 'Type Input', required=True)
    frecuency_kind = fields.Many2One('maintenance.planning.frecuency_kind',
        'Frecuency Kind')

    @classmethod
    def __setup__(cls):
        super(MeterReading, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints += [
            ('code_uniq', Unique(table, table.code),
                'Code must be unique by definition!'),
        ]

    def get_rec_name(self, name):
        if self.uom_reading:
            return self.name + ' [' + self.uom_reading.symbol + ']'
        else:
            return self.name

    @staticmethod
    def default_type_input():
        return 'incremental'

    @staticmethod
    def default_cost_value():
        return Decimal('0.0')

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            (cls._rec_name,) + tuple(clause[1:]),
            ]

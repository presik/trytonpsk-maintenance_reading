# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import input_reading
from . import meter_reading
from . import schedule
from . import planning
from . import configuration
from . import shipment


def register():
    Pool.register(
        configuration.Configuration,
        meter_reading.MeterReading,
        input_reading.InputReading,
        input_reading.ReadingReport,
        schedule.Schedule,
        planning.FrecuencyKind,
        input_reading.LoadGpsDataStart,
        shipment.ShipmentInternal,
        module='maintenance_reading', type_='model')
    Pool.register(
        input_reading.LoadGpsData,
        module='maintenance_reading', type_='wizard')

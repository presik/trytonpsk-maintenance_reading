# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class Configuration(metaclass=PoolMeta):
    __name__ = 'maintenance.configuration'
    url_api = fields.Char('Url api')
    api_key = fields.Char('Api Key')

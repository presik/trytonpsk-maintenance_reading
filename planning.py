# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class FrecuencyKind(metaclass=PoolMeta):
    __name__ = 'maintenance.planning.frecuency_kind'
    meter_reading = fields.Many2One('maintenance.meter_reading',
            'Meter Reading', states={
                'invisible': Eval('source_data') != 'input_reading',
                'required': Eval('source_data') == 'input_reading',
    })

    @classmethod
    def __setup__(cls):
        super(FrecuencyKind, cls).__setup__()
        new_sel = [
                ('input_reading', 'Input Reading'),
        ]
        if new_sel not in cls.source_data.selection:
            cls.source_data.selection.extend(new_sel)
